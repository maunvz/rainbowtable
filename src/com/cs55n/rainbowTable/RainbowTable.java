package com.cs55n.rainbowTable;


/* This class needs a data structure to hold all the
 * start/end points for the generated chains, a searching
 * function, and a way to save to file and load from file
 */

public class RainbowTable {
	//store pairs - start(plain text) and end(hash)
	//one pair is byte[][] where byte[0][] is the start and byte[1][] is the end
	byte[][][] chains;
	//last index that is created, used when loading and creating a table
	int lastIndex;
	int steps;
	int index;//since we support loading multiple tables, this keeps track of which table is which
	boolean ready;//true when table is fully loaded
	public RainbowTable(int n, int steps, int index){
		chains = new byte[n][][];
		lastIndex=0;
		this.steps = steps;
		this.index=index;
		ready=false;
	}
	//adds a chain to the array
	public void addChain(byte[][] chain){
		chains[lastIndex++]=chain;
	}
}

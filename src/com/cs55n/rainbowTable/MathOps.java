/*
 * This class needs to hold the hash, reduction,
 * and other necessary functions. We will do all the
 * operations on byte[] instead of String because it
 * is faster to do it that way
 */

package com.cs55n.rainbowTable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class MathOps {
	int passwordLength;
	MessageDigest digest;
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	Random rand;
	public MathOps(int passwordLength){
		this.passwordLength = passwordLength;
		try{ digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e){}
		rand = new Random();
	}
	//Returns true if arr1 and arr2 have the same bytes
	public static boolean bytesEqual(byte[] arr1, byte[] arr2){
		for(int i=0; i<arr1.length&&i<arr2.length; i++){
			if(arr1[i]!=arr2[i])return false;
		}
		return true;
	}
	//returns a 32-byte hash of bytes
	byte[] hash(byte[] strBytes){
		return digest.digest(strBytes);
	}
	//returns a valid password representation of hash based on k (the step in the chain)
	byte[] reduce(byte[] bytes, int k){
		int mask = 1;
		for(int i=0; i<passwordLength; i++)mask*=2;
		mask-=1;
		mask = mask << 32-passwordLength;
		mask = mask | k; 
		//creates an int whose 32-bit representation has at least passwordLength 1's in it
		//the position of the 1's represents the position of the bytes in the hash that are used for the reduction
		
		byte[] reduced = new byte[passwordLength];
		int byteNo = 0;
		for(int i=0; i<32&&byteNo<passwordLength; i++){
			if((mask & 1) == 1){
				//we want values between 97 and 122 (which represent the lower case letters)
				reduced[byteNo++] = (byte)(97+((bytes[i]+128)%26));
			}
			mask = mask >> 1;
		}
		return reduced;
	}
	public static String hexToString(String hex) {
	    StringBuilder output = new StringBuilder();
	    for (int i = 0; i < hex.length(); i+=2) {
	        String str = hex.substring(i, i+2);
	        output.append((char)Integer.parseInt(str, 16));
	    }
	    return output.toString();
	}	
	//converts bytes to a hex string for output
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	//converts a hex string to a byte array for input
	public static byte[] hexToBytes(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
	//byte array to int, where b is 4 long
	public static int bytesToInt(byte[] b){
	    int value = 0;
	    for (int i = 0; i < 4; i++) {
	        int shift = (4 - 1 - i) * 8;
	        value += (b[i] & 0x000000FF) << shift;
	    }
	    return value;
	}
	//returns 1 if b1 is greater, -1 if b2 is greater, 0 is equal
	public static int compareBytes(byte[] b1, byte[] b2){
		for(int i=0; i<b1.length&&i<b2.length;i++){
			if(b1[i]>b2[i])return 1;
			if(b2[i]>b1[i])return -1;
		}
		return 0;
	}
	//copy an array of bytes
	public static byte[] copyBytes(byte[] arr){
		byte[] copy = new byte[arr.length];
		for(int i=0; i<arr.length; i++){
			copy[i]=arr[i];
		}
		return copy;
	}
}